#!/bin/sh
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: NONE

EDITIDE_VERSION=${CI_COMMIT_TAG}
EDITIDE_DATE=$(git show --no-patch --format='%cs')

if test -n "${EDITIDE_VERSION}"; then
	sed -i -E "s/version: '\S+?'/version: '${EDITIDE_VERSION}'/g" ./meson.build
	sed -i -E "s/version = \"\S+?\"/version = \"${EDITIDE_VERSION}\"/g" ./pyproject.toml
	sed -i -E "s/release version=\"\S+?\"/release version=\"${EDITIDE_VERSION}\"/g" ./data/io.frama.editide.EdiTidE.metainfo.xml
	sed -i -E "s/date=\"\S+?\"/date=\"${EDITIDE_DATE}\"/g" ./data/io.frama.editide.EdiTidE.metainfo.xml
	sed -i -E "s/VERSION = '\S+?'/VERSION = '${EDITIDE_VERSION}'/g" ./src/editide/__init__.py
	sed -i -E "s/\"ProductVersion\", \"\S+?\"/\"ProductVersion\", \"${EDITIDE_VERSION}\"/g" ./src/editide/win32/version.rc
fi

sed -i "s/ICON = 'editide'/ICON = 'io.frama.editide.EdiTidE'/g" ./src/editide/__init__.py
sed -i "s/localhost\.EdiTidE4/io.frama.editide.EdiTidE/g" ./src/editide/__main__.py

exit 0
