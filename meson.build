project('editide',
  version: '4.dev0',
  meson_version: '>= 1.1',
  license: 'CC0-1.0',
)

app_id = 'io.frama.editide.EdiTidE'

# Build dependencies
pymod = import('python')
gnome = import('gnome')

python3 = pymod.find_installation('python3')
if not python3.found()
  error('Python 3 is required')
endif
pip3 = find_program('pip3', required: false)

# Options
prefix = get_option('prefix')
datadir = get_option('datadir')
destdir = get_option('destdir')
icondir = datadir / 'icons' / 'hicolor'

# Install python module
if pip3.found()
  # Install with pip
  if destdir == ''
    meson.add_install_script(
      pip3, 'install', '--no-deps', '--no-build-isolation',
      '--prefix=@0@'.format(prefix),
      meson.current_source_dir()
    )
  else
    meson.add_install_script(
      pip3, 'install', '--no-deps', '--no-build-isolation',
      '--prefix=@0@'.format(prefix),
      '--root=@0@'.format(destdir),
      meson.current_source_dir()
    )
  endif
else
  # Prepare wheel
  meson.add_install_script(
    python3, '-m', 'build', '--wheel', '--skip-dependency-check', '--no-isolation',
    '--outdir=@0@'.format(meson.current_build_dir()),
    meson.current_source_dir()
  )
  # Install wheel
  if destdir == ''
    meson.add_install_script(
      python3, '-m', 'installer', '--prefix=@0@'.format(prefix),
      meson.current_build_dir() / 'EdiTidE-@0@-py3-none-any.whl'.format(meson.project_version())
    )
  else
    meson.add_install_script(
      python3, '-m', 'installer', '--prefix=@0@'.format(prefix), '--destdir=@0@'.format(destdir),
      meson.current_build_dir() / 'EdiTidE-@0@-py3-none-any.whl'.format(meson.project_version())
    )
  endif
endif

# Install icon
install_data(
  'src' / 'editide' / 'data' / 'icons' / 'hicolor' / 'scalable' / 'apps' / 'editide.svg',
  install_dir: icondir / 'scalable' / 'apps',
  rename: '@0@.svg'.format(app_id),
)

# Install symbolic icon
install_data(
  'src' / 'editide' / 'data' / 'icons' / 'hicolor' / 'symbolic' / 'apps' / 'editide-symbolic.svg',
  install_dir: icondir / 'symbolic' / 'apps',
  rename: '@0@-symbolic.svg'.format(app_id),
)

subdir('data')
subdir('po')

gnome.post_install(
  gtk_update_icon_cache: true,
  update_desktop_database: host_machine.system() not in ['darwin', 'windows'],
)
